/*
This is part 4 of the in class assignment. This grabs 10 characters from the user and prints them back out to the terminal.
Author: Sanat Deshpande
Date Created: 9/14/16
 */

#include <stdio.h>

int main(){

  int count = 0;
  printf("Enter in text\n");
  while(count < 10){
    char out = getchar();
    count++;
    printf("%c",out);
  }
  return 0;
}
