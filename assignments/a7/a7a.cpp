/*
Sanat Deshpande

This is the main file for the bigram analyzer. This file
takes in an input sentence and deconstructs it into bigram
frequencies. 
*/

#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

void makeBigram(vector<string>* bigram,string sentence); //generates bigram vector from sentence
void tally(vector<string>* bigram); //tallies bigram frequencies and deletes duplicates
void freqSort(vector<string>* bigram); //sorts by bigram frequency

int main(int argc, char* argv[]){
  string sentence; //to hold model sentence

  //Gets input from file
  if(argc != 3){
    cout<<"Invalid Number of Arguments. Program Terminated"<<endl;
    return 0;
  }
  else if(*argv[1] != 'a' && *argv[1] != 'r' && *argv[1] != 'c'){
    cout<<"Invalid Arguments. Program Terminated"<<endl;
    return 0;
  }
  
  ifstream inFile;
  inFile.open(argv[2]);
  getline(inFile,sentence);
  inFile.close();
 
  sentence  = "<START> " + sentence +  " <END>";
  //******************************************

  vector<string>* bigrams = new vector<string>();
  makeBigram(bigrams,sentence); //generates string vector containing consecutive word pairs
  tally(bigrams); //tallies bigram frequencies and deletes duplicates

  if(*argv[1] == 'a'){
    sort(bigrams->begin(),bigrams->end());
  }
  else if(*argv[1] == 'r'){
    sort(bigrams->begin(),bigrams->end());
    reverse(bigrams->begin(),bigrams->end());
  }
  else if(*argv[1] == 'c'){
    freqSort(bigrams);
    
  }
  else{
    cout<<"Invalid Arguments. Program Terminated"<<endl;
  }
  

  
  for(vector<string>::iterator i = bigrams->begin(); i != bigrams->end(); i++){
    cout<<*i<<endl;
  }
  
   
  return 0;
}

/*
  Sorts string vector by bigram frequency
*/
void freqSort(vector<string>* bigram){
  for(vector<string>::iterator i = bigram->begin(); i != bigram->end(); i++){
    for(int j = i->length()-1; j >= 0; j--){
      if( (*i)[j] == ' '){
	(*i) = i->substr(j+1,i->length()) + " " + (*i);
	break;
      }
    }
  }
  sort(bigram->begin(), bigram->end());
  for(vector<string>::iterator i = bigram->begin(); i != bigram->end(); i++){
    for(unsigned int j = 0; j < i->length(); j++){
      if( (*i)[j] == ' '){
	(*i) = i->substr(j+1,i->length());
	break;
      }
    }
  }
}
/*
 Takes in a string vector pointer containing bigram pairs, and modifies the strings within to reflect
 the appropriate bigram frequencies. Duplicate strings are deleted.
 */
void tally(vector<string>* bigram){
  int count = 0; //initial minimum frequency

  for(vector<string>::iterator i = bigram->begin(); i != bigram->end(); i++){ //iterates through vector
    for(vector<string>::iterator j = i; j != bigram->end(); j++){
      if(i->compare(*j) == 0){
	  count++; //increments count
	  if(count > 1){ //if string not comparing to itself
	    bigram->erase(j); //deletes the dupliate string and sets back the index
	    j--;
	  }
      }
    }
    (*i) += " " + to_string(count); //adds count to output string
    count = 0; //resets count
  }  
}

/*
 Takes in an empty string vector pointer and the sample sentence, and parses the sentence
 as bigram pairs to store in the string vector.
 */
void makeBigram(vector<string>* bigram,string sentence){
  bigram->push_back(""); //creates first element of vector

  for(unsigned int j = 0; j < sentence.length(); j++){ //places each word of sentence into vector
    if(sentence[j] == ' '){
      bigram->push_back("");
    }
    else{
      bigram->back() += sentence[j];
    }
  }

  for(unsigned int j = 0; j < bigram->size() - 1; j++){ //creates bigram pairs
    bigram->at(j) += " " + bigram->at(j+1);
  }
  bigram->erase(bigram->end()-1);
}
