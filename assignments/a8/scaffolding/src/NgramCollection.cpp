/*
This file hanldes the creation and storage of Ngram models.
*/

#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <cstdlib>
#include "../include/NgramCollection.hpp"

using namespace std;

void NgramCollection::increment(vector<string>::const_iterator begin, vector<string>::const_iterator end){
  vector<string> preceeding;
  string nth;
  for(auto i = begin; i != end-1; i++){
    preceeding.push_back(*i); //generates string vector
  }
  nth = *(end-1); //generates string
  counts[preceeding][nth]++;
  cout<<nth<<" "<<counts[preceeding][nth]<<endl;
}
