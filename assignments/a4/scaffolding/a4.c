/**************************************
 * a4.c
 *
 * Sanat Deshpande
 * October 1, 2016
 *
 * Program for solving word search puzzles.
 *
 *
 * Scaffolding for CS120-F16, HW4 - fill in
 * main, but keep it short; my reference implementation
 * has about 20 lines of actual code in main (not counting
 * comments and whitespace).
 *************************************/

#include <stdio.h>
#include "wordsearch.h"
#include <string.h>


int main (int argc, char* argv[]) {

  if(argc != 2){ //handles for unexpected number of arguments
    return 0;
  }
  FILE *filehandler = fopen(argv[1],"r");
  char puzzle[10][10]; //stores puzzle
  int dim = isValid(filehandler,puzzle); //populates puzzle 2D array and verifies validity. also returns dimensions
  if(dim == 0){ //dim is 0 for invalid puzzles. this terminates the program
    return 0;
  }

  char term[11];
  while( scanf("%s",term) != EOF){
    validateInput(term,puzzle,dim); //ensures all input is lower-case
    search(term,puzzle,dim); //searches for term and prints out results
  }

  return 0;
}
