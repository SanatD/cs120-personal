/*
This file is for part 3 of the in class assignment (week2)
Author: Sanat Deshpande
Date Created: 9/14/16

*/

#include <stdio.h>

int main(){
  int num = 1;
  int val = 1; 
  while (num < 6){
    num++;
    val = val * num;
    printf("%d\n%d\n",num,val);
  }
  return 0;
}
