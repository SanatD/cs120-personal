/* wordsearch.c
 *
 * Sanat Deshpande
 * October 3, 2016
 *
 * Functions needed for a word-search puzzle solver.
 *
 * Scaffolding for CS120-F16, A4 - make at least 4 functions
 */

#include "wordsearch.h"
#include <string.h>
#include <stdio.h>

/**
Searches through the puzzle for the given term. The function pulls substrings in every
direction (not diagonal) of each character in the puzzle, and compare it to the search term.
The case that no matches are found is also reported.
**/
void search(char term[11], char puzzle[10][10], int dim){

  int len = strlen(term); //stores length of search term
  char subString[len+1]; //substring of puzzle being compared to user input
  int found = 0; //0 if term not found, 1 if term is found
  for(int i = 0; i < dim; i++){ //i and j loop iterate through each character of the puzzle
    for(int j = 0; j < dim; j++){
      for(int dir = 0; dir < 4; dir++){ //iterates through each direction a word may be in
	int row = i; //starting position for generating substring to compare to search term
	int col = j;
        int rowStep = 0; //increments by which to build substrings. Enables movement in any of 4 directions
        int colStep = 0;
        int index = 0; //for substring
	char direction; //stores direction to be printed out
	if(dir == 0){ //UP
	  rowStep = -1;
	  colStep = 0;
	  direction = 'U';
	}
	else if(dir == 1){ //RIGHT
	  rowStep = 0;
	  colStep = 1;
	  direction = 'R';
	}
	else if(dir == 2){ //DOWN
	  rowStep = 1;
	  colStep = 0;
	  direction = 'D';
	}
	else if(dir == 3){ //LEFT
	  rowStep = 0;
	  colStep = -1;
	  direction = 'L';
	}

	while(index < len){ //builds substring for potential match using rowStep/colStep direction
	  if(row > (dim-1) || row < 0 || col > (dim-1) || col < 0){ //break loop if out of bounds of puzzle
	    break;
	  }
	  subString[index] = puzzle[row][col];
	  row += rowStep;
	  col += colStep;
	  index++;
	}
	subString[index] = '\0'; //makes subString null-terminated
	if(compare(term,subString) == 1){
	  printf("Found \"%s\" at %d %d, %c\n",term,i,j,direction); //prints results
	  found = 1; //signals that a term has been found
	}
      } //end 'dir' loop
    } //end 'j' loop
  } //end 'i' loop
  if(found != 1){ //print not found message
    printf("\"%s\" - Not Found\n",term);
  }
}
/**
This takes in two character arrays and returns 1 if they're a match, and 0 otherwise.
**/
int compare(char one[11],char two[11]){
  if(strlen(one) != strlen(two)){
    return 0;
  }
  for(unsigned int i = 0; i < strlen(one);i++){
    if(one[i] != two[i]){
      return 0; //0 signifies no match
    }
  }
  return 1; //1 signifies a match
}
/**
This runs through the give puzzle file and ensures that it is valid. 
Validity necesitates that the puzzle is square, no larger than 10x10, and at least 1x1.
Also verfies that all rows are the same length. 
It returns 0 if puzzle is invalid, and the dimension of the square puzzle otherwise.
**/
int isValid(FILE *f,char puzzle[10][10]){
  char in;
  int rows = 0; //keeps track of rows
  int cols = 0; //keeps track of columns
  int cols2 = 0; //secondary column tracker
  int unequal = 0; //keeps track of whow many times the rows are unequal lengths
  //char puzzleChars[100]; //stores all puzzle characters for transferrance to 2D array
  int i = 0; //index for puzzleChars
  int j = 0;
  while ((in = fgetc(f)) != EOF){ //iterates through puzzle and counts rows
    if(in != '\n'){
      puzzle[i][j] = in; //populates puzzle 2D array
      j++; //increments column
      cols++;
    }
    if(in == '\n'){
      rows++;
      i++; //increments row
      j = 0; //resets column
      if(cols2 != cols){
	unequal++;
      }
      cols2 = cols; //sets secondary tracker to previous value
      cols = 0; //resets column counter
    }
  }

  i = 0; //resets index for puzzleChars

  if(unequal > 1){ //if rows are unequal
    return 0;
  }
  if(rows > 10 || rows < 1){ //if too many/few rows/cols
    return 0;
  }
  if(rows != cols2){ //if number of rows and cols doesn't match
   return 0;
  }
  return rows; //returns 1 to signify a valid puzzle
}
/**
Assuming the input puzzle has been marked as valid, this function takes the input and the puzzle
and ensures all letters are lowercase. This avoid issues related to case-sensitivity.
**/
void validateInput(char term[11], char puzzle[10][10],int dim){
  for(unsigned int i = 0; i < strlen(term); i++){
    term[i] = tolower(term[i]);
  }
  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      puzzle[i][j] = tolower(puzzle[i][j]);
    }
  }
}
