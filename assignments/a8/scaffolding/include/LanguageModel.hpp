/*
This is the header file for LanguageModel.cpp
*/
#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <cstdlib>
#include "NgramCollection.hpp"
using namespace std;

class LanguageModel{

public:
  LanguageModel(unsigned num, string f) : n(num), filename(f) {} //constructor
  vector<string> textToVector(string text); //takes a string and parses it into a vector
  vector<string> getFileList(string filename); //gets list of files to use as string vector
  string getText(string filename); //gets text from file
  void buildCollection(); //uses NgramCollection object to build collection
  void test(); //test
private:
  unsigned n;
  string filename;
  NgramCollection coll = NgramCollection(n);

};

