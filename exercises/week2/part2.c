/*
This file is for an in-class assignment (week 2)
Author: Sanat Deshpande
Date Created: 9/14/16
 */

#include <stdio.h>

int main(){
  int age = 18;
  printf("Hello, world, my name is Sanat Deshpande, and my age is %d",age);
  return 0;
}
