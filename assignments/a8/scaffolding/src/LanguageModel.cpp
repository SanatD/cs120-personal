/*
This class will house the functionality to process the sample texts to generate models,
and to use those models to generate sentences
*/


/*
TO DO:
1. Write a parseFile function that uses the already defined functions to generate a vector from the initial input file
2. Make LanguageModel.cpp into a class. It must have an NgramModel object as an instance variable.
3. Fully flesh out all methods relevant to forming the model from the NgramCollection.hpp. (working on the cpp).
*/

#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <cstdlib>
#include "../include/LanguageModel.hpp"

using namespace std;

void LanguageModel::test(){
  cout<<n<<endl;
}

void LanguageModel::buildCollection(){
  vector<string> files = getFileList(filename);
  for(vector<string>::iterator j = files.begin()+1; j != files.end(); j++){
    vector<string> words = textToVector(getText(*j));

    for(vector<string>::iterator i = words.begin(); (i+n-1) != words.end(); i++){
      vector<string>::const_iterator start = i;
      vector<string>::const_iterator end = (i+n-1);
      coll.increment(start,end);
    }
  }
  
 
}

vector<string> LanguageModel::getFileList(string fname){
  vector<string> list;
  ifstream inFile;
  inFile.open(fname);
  string data;
  while(getline(inFile,data)){
    list.push_back(data);
  }
  inFile.close();
  return list;
}

string LanguageModel::getText(string fname){
  ifstream inFile;
  inFile.open(fname);
  string data = "";
  string text = "";
  while(getline(inFile,data)){
    //cout<<"data: "<<data<<endl;
    //cout<<"text: "<<text<<endl;
    text += data;
  }
  inFile.close();
  //cout<<"prior text: "<<text<<endl;
  return text;
}

vector<string> LanguageModel::textToVector(string text){
  vector<string> words;
  string singleWord = "";
  //cout<<"text: "<<text<<endl;
  for(int i = 0; i < text.length(); i++){ //builds vector
    if(text[i] == ' '){
      words.push_back(singleWord);
      singleWord = "";
    }
    else{
      singleWord += text[i];
    }
    //cout<<"word: "<<singleWord<<endl;
  }
  words.push_back(singleWord); //adds last word
  for(vector<string>::iterator i = words.begin(); i != words.end(); i++){
    if((*i).length() == 0 || isspace((*i)[0]) ){
      words.erase(i);
      i--;
    }
    
  }
  return words;
}

