/*
This is phase 2. It prints out a line of dashes followed by the character count of an input.
Authors: Gurion Marks and Sanat Deshpande
Date Created: 9/16/16
Date Modified: 9/16/16
*/
#include <stdio.h>

int main(){

  char c = getchar();
  int count = 0;
  printf("-------------\n");
  while(c != EOF){
    count++;
    c = getchar();
  }
  printf("%d characters\n",count);
  return 0;
}
