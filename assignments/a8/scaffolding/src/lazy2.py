import os
import sys

files = os.listdir(os.path.dirname(os.path.realpath(__file__)))
#files = os.listdir(sys.argv[0])
#print(sys.argv[0])

for i in files:
    if i.split(".")[-1] == "cpp":
        command = "g++ -Wall -Wextra -pedantic -std=c++11 -o " + i[:-4] + " " + i
        os.system(command)
    
