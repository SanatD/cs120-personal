/*
Sanat Deshpande

This program takes in an input file containing bigrams and their frequencies.
It uses them as a model to generate random sentences based on the bigrams.
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <cstdio>
#include <ctime>

using namespace std;

void expand(vector<string>* model); //duplicates list elements according to frequency, and trims trailing numbers from string
void generateSentence(vector<string>* model); //generates a sentence based on the model
string getFirst(string sentence); //gets first word in sentences
string getLast(string sentence); //gets last word in sentence

int main(int argc, char* argv[]){
  srand(time(0)); //random number seeding
  //Gets input from file
  if(argc != 3){
    cout<<"Invalid Number of Arguments. Program Terminated"<<endl;
    return 0;
  }
  cout<<"one"<<endl;
  ifstream inFile;
  inFile.open(argv[2]);
  string line; //stores line-by-line of input file
  vector<string>* model = new vector<string>();
  while(getline(inFile,line)){
    model->push_back(line);
    cout<<model->at(0)<<endl;
  }
  inFile.close();
  cout<<model->size()<<endl;
  cout<<"two"<<endl;
  //The following generates the sentence from the model
  expand(model); //expands the model by bigram frequency
  cout<<"three"<<endl;
  for(int i = 0; i < stoi(argv[1]); i++){ //randomly generates sentence a number of times specified by the user
    cout<<"four"<<endl;
    generateSentence(model);
    cout<<"five"<<endl;
  } 
  cout<<"six"<<endl;
  return 0;
}

/*
Gets last word in string
*/
string getLast(string sentence){
  string lastWord;
  for(int i = sentence.length()-1; i >= 0; i--){
    if( sentence[i] == ' '){
      lastWord = sentence.substr(i+1,sentence.length());
      return lastWord;
    }
  }
  return "";
}

/*                                                                                                                                                           
Gets first word in string                                                                                                                                   
*/
string getFirst(string sentence){
  string firstWord;
  for(unsigned int i = 0; i < sentence.length(); i++){
    if( sentence[i] == ' '){
      firstWord = sentence.substr(0,i);
      return firstWord;
    }
  }
  return "";
}

/*
Takes in a model processed by the expand function and generates a sentence from it
*/
void generateSentence(vector<string>* model){
  map<string,vector<string>> bigram;
  string sentence = "";
  cout<<"gen 1"<<endl;
  cout<<model->at(0)<<endl;
  for(vector<string>::iterator i = model->begin(); i != model->end(); i++){
    bigram[getFirst(*i)].push_back(getLast(*i));
  }
  cout<<"gen 2"<<endl;
  cout<<model->size()<<endl;
  sentence += model->at(0); //initializes sentence
  cout<<"gen 3"<<endl;
  string lastWord = "";
  cout<<"Asdf"<<endl;
  while(getLast(sentence) != "<END>"){
    cout<<"gen 4"<<endl;
    lastWord = getLast(sentence);
    int vectorSize = bigram[lastWord].size();
    int random = rand() % vectorSize;
    sentence += " " + bigram[lastWord].at(random);
    cout<<"gen 5"<<endl;
  }
  cout<<sentence<<endl; 
}

/*
Expands list according to frequencies, and trims numbers off the strings. 
*/
void expand(vector<string>* model){
  int count = 0; //number of copies to make
  string trimmed = ""; //model without frequency
  cout<<"expand1"<<endl;
  for(vector<string>::iterator i = model->begin(); i != model->end(); i++){
    for(int j = i->length()-1; j >= 0; j--){ //gets number of times to duplicate 
      if( (*i)[j] == ' '){
	count = stoi(i->substr(j+1,i->length())); //gets count
	(*i) = i->substr(0,j); //trims frequency number from bigram
	cout<<count<<endl;
	break;
      }
    }
    for(int k = 0; k < count-1; k++){ //makes "count" copies of elements
       model->insert(i,(*i));  	
    }
    i+=count-1; //advances iterator over copies  
  }
}
