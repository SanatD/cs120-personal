/*
This is phase 1. It prints out every character of a given input.
Authors: Gurion Marks and Sanat Deshpande
Date Created: 9/16/16
Date Modified: 9/16/16
*/
#include <stdio.h>

int main(){

  char c = getchar();

  while(c != EOF){
    printf("%c",c);
    c = getchar();
  }
  
  return 0;
}
