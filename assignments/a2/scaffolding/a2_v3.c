/*
This is phase 3. It prints out a count of all of the characters, words, and lines.
Authors: Gurion Marks and Sanat Deshpande
Date Created: 9/16/16
Date Modified: 9/16/16
*/
#include <stdio.h>

int main(){

  char c = getchar();
  int charCount = 0;
  int isWord = 0; //this is a pseudo-boolean. 0 = false, 1 = true
  int wordCount = 0;
  printf("-------------\n");
  while(c != EOF){ //loops through all input
    while(!isspace(c)){ //determines whether space-separated character groups are "words"
      charCount++;
      if(!ispunct(c)){
	isWord = 1; //flags this group of characters as being a word (as opposed to a collection of punct)
      }
      c = getchar();
    }
    charCount++;
    if(isWord == 1){ //increments word count if word is found. resets "isWord" to "false"
      wordCount++;
      isWord = 0;
    }
    c = getchar();
  }
  printf("%d Characters\n%d words\n",charCount,wordCount);
  return 0;
}
