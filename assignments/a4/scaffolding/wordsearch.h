/* wordsearch.h
 *
 * Sanat Deshpande
 * October 3, 2016
 *
 * Headers for functions needed for a word-search puzzle solver
 *
 * Scaffolding for CS120-F16, A4 - put function prototypes here
 */

#ifndef _CS120_SCAFFOLD_WORDSEARCH_H
#define _CS120_SCAFFOLD_WORDSEARCH_H

#include <stdio.h> // you'll need this if you want file handles as arguments
#include <string.h>
#include <stdio.h>

#define MAX_GRID_SIZE 10 // constant for max grid size
#define WORD_BUFFER_LENGTH 11 // space for 10 "word" chars plus a null-termination

void search(char term[11], char puzzle[10][10], int dim); //searches for match
int compare(char one[11], char two[11]); //compare two char arrays
int isValid(FILE *f, char puzzle[10][10]); //checks if puzzle is valid
void validateInput(char term[11], char puzzle[10][10],int dim); // makes input and puzzle lowercase

#endif
